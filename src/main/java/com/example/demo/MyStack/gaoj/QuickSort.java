﻿package com.example.demo.MyStack.gaoj;

import com.alibaba.fastjson.JSON;
import com.example.demo.order.ArrayUtil;

public class QuickSort {
    public static void main(String[] args) {
        ArrayUtil arrayUtil = new ArrayUtil();
        int[] array = arrayUtil.getArray(10);
        quickSort(array, 0, array.length);   // 这里需要注意  这里的length 关系到后面判断是否是array[i] < mid  或者 array[i] <= mid  后面递归的时候也需要注意
        System.out.println(JSON.toJSONString(array));
    }

    private static void quickSort(int[] array, int left, int right) {
        if (left >= right) {
            return;
        }
        ArrayUtil arrayUtil = new ArrayUtil();
        int l = left;
        int r = right;
        int j = left;
        int mid = array[left];
        for (int i = l + 1; i < r; i++) {
            if (array[i] < mid && j + 1 != i) {
                arrayUtil.swap(++j, i, array);
            }
        }
        arrayUtil.swap(j, l, array);
        System.out.println(j);
        System.out.println("---" + JSON.toJSONString(array));
        quickSort(array, 0, j);
        quickSort(array, j + 1, r);
    }


}
