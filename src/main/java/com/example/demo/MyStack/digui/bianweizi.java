package com.example.demo.MyStack.digui;

import com.alibaba.fastjson.JSON;

public class bianweizi {
    private static char[] words;
    private static int count;

    public static void main(String[] args) {
        String string = "kxz";
        words = string.toCharArray();
        doAnagram(words.length);
    }

    private static void doAnagram(int newSize) {
        if (newSize == 1)
            return;
        for (int j = 0; j < newSize; j++) {
            doAnagram(newSize - 1);
            if (newSize == 2)
                displayWord();
            rotate(newSize);
        }
    }


    private static void rotate(int newSize) {
        int j = words.length - newSize;
        char temp = words[j];
        for (; j + 1 < words.length; j++) {
            words[j] = words[j + 1];
        }
        words[j] = temp;
    }


    private static void displayWord() {
        System.out.print(++count + ":");
        System.out.print(JSON.toJSONString(words));
        System.out.print("   ");
        if (count % 5 == 0)
            System.out.println("");
    }
}
