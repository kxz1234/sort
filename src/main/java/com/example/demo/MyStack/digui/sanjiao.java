package com.example.demo.MyStack.digui;

public class sanjiao {
    private static int total;
    public static void main(String[] args) {
        int n = 5;
        while (n > 0) {
            for (int i = 0; i < n; i++) {
                System.out.print("*");
            }
            System.out.println();
            n--;
        }
        System.out.println("-------------------------------------");
        for (int j = 1; j <= 5; j++) {
            for (int m = 0; m < j; m++) {
                System.out.print("*");
            }
            System.out.println();
        }

        System.out.println("-------------------------------------");
        total = 0;
        System.out.println(add(4));
        System.out.println("====================================");
        total = 0;
        System.out.println("阶乘:" + multi(8));

    }

    private static int add(int n) {
        if (n > 0) {
            total += n;
            return add(n - 1);
        } else {
            return total;
        }
    }

    private static int multi(int n) {
        if (n > 0) {
            total += (n - 1) * n;
            System.out.println(total);
            return multi(n - 1);
        } else {
            return total;
        }
    }
}
