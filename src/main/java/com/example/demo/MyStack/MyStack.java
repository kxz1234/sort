package com.example.demo.MyStack;

import java.util.*;

public class MyStack {
    public static void main(String args[]) {
        Stack<Object> objectStack = new Stack<Object>();
        if (!objectStack.empty()) {
            objectStack.pop();
        }
        StackUtil stackUtil = new StackUtil(3);

        System.out.println("isFull: " + stackUtil.isFull());
        stackUtil.push(1l);
        stackUtil.push(2l);
        stackUtil.push(3l);
        System.out.println("isFull: " + stackUtil.isFull());
        stackUtil.push(4l);
        System.out.println("isFull: " + stackUtil.isFull());
        System.out.println("pop: " + stackUtil.pop());
        System.out.println("peek: " + stackUtil.peek());

        try {

        } catch (EmptyStackException e) {
            System.out.println("empty stack");
        }
    }

}
