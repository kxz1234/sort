package com.example.demo.MyStack.string;
import java.util.EmptyStackException;

public class CharStackUtil {
    private int maxsize;
    private char[] stackarray;
    private int top;

    public CharStackUtil(int maxsize) {
        this.top = -1;
        stackarray = new char[maxsize];
        this.maxsize = maxsize;
    }

    public synchronized void push(char j) {
        if(isFull()) throw new RuntimeException();
        stackarray[++top] = j;
    }
    public synchronized char pop() {
        if (top == -1) {
            throw new EmptyStackException();
        }
        return stackarray[top--];
    }

    public synchronized char peek() {
        if (stackarray.length == 0) throw new RuntimeException();
        return stackarray[top];
    }

    public synchronized  Boolean empty() {
        return top == -1;
    }

    public int search(char j) {
        for (int i = 0; i < stackarray.length; i++) {
            if (stackarray[i] == j) {
                return i;
            }
        }
        return -1;
    }

    public Boolean isFull() {
        return maxsize - 1 == top;
    }
}
