package com.example.demo.MyStack.string;

public class revers {
    private String input;
    private String outPut;

    public revers(String input) {
        this.input = input;
    }

    public String deRev() {
        int size = input.length();
        CharStackUtil charStackUtil = new CharStackUtil(size);
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            charStackUtil.push(c);

        }
        outPut = "";
        while (!charStackUtil.empty()) {
            char ch = charStackUtil.pop();
            outPut = outPut + ch;
        }
        return outPut;

    }
}
