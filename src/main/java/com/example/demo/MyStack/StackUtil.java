package com.example.demo.MyStack;
import java.util.EmptyStackException;

public class StackUtil {
    private int maxsize;
    private Long[] stackarray;
    private int top;

    public StackUtil(int maxsize) {
        this.top = -1;
        stackarray = new Long[maxsize];
        this.maxsize = maxsize;
    }

    public synchronized void push(Long j) {
        if(isFull()) throw new RuntimeException();
        stackarray[++top] = j;
    }
    public synchronized Long pop() {
        if (top == -1) {
            throw new EmptyStackException();
        }
        return stackarray[top--];
    }

    public synchronized Long peek() {
        if (stackarray.length == 0) throw new RuntimeException();
        return stackarray[top];
    }

    public synchronized  Boolean empty(Long j) {
        return top == -1;
    }

    public int search(Long j) {
        for (int i = 0; i < stackarray.length; i++) {
            if (stackarray[i] == j) {
                return i;
            }
        }
        return -1;
    }

    public Boolean isFull() {
        return maxsize - 1 == top;
    }
}
