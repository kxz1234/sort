package com.example.demo;

public class box<T extends base> {

    private String name;

    public box(String name, T data) {
        this.name = name;
        this.data = data;
    }

    private T data;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public box(T data) {
        this.name = "kxz";
        this.data = data;
    }
}
