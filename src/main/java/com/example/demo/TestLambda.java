package com.example.demo;

import com.alibaba.fastjson.JSON;

import java.util.*;
import java.util.stream.Collectors;

public class TestLambda {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
//        list.forEach(x -> System.out.println(x));
        long a = list.stream().map(x-> x+1).count();
        System.out.println(a);
//        list.stream().map(x-> x+1).forEach(x->System.out.println(x));
        List<Integer> list3 =  list.stream().filter(x -> x>3).collect(Collectors.toList());
        System.out.println(JSON.toJSONString(list3));
    }

}
