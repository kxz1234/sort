package com.example.demo.order;

import com.alibaba.fastjson.JSON;

import java.util.Random;

public class ArrayUtil {
    public int[] getArray(int num) {
        Random r = new Random();
        int[] intArray = new int[num];
        int i = 0;
        while (true) {
            intArray[i] = r.nextInt(100);
            i++;
            if (i == num) break;
        }
        System.out.println("数组:" + JSON.toJSONString(intArray));
        return intArray;
    }

    public void swap(int a, int b, int[] array) {
        if (a == b) return;
        int temp = array[a];
        array[a] = array[b];
        array[b] = temp;
    }
}
