package com.example.demo.order;

import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

//        List list =new ArrayList();
//        Collections.sort(list);
//        list.sort(null);
public class charuOrder {
    public static void main(String[] args) {
        ArrayUtil arrayUtil = new ArrayUtil();
        int[] array = arrayUtil.getArray(100);
        for (int i = 1; i < array.length; i++) {
            int k = i;
            int temp = array[i];
            while (k > 0 && array[k - 1] > temp) {
                array[k] = array[k - 1];  //前一个数比这个数大就往后移一位
                k--;
            }
            array[k] = temp;  // 最终结果
        }
        System.out.println("排序之后："+JSON.toJSONString(array));
        dereplication(array);
    }

    private static void dereplication(int[] array) {
        System.out.println("===========开始去重");
        int index = 0;
        int i = 0;
        while (i < array.length) {
            array[index] = array[i];
            i = isEq(i, array);
            i++;
            index++;
        }
        int[] newArray = new int[index];
        System.arraycopy(array, 0, newArray, 0, index);
        System.out.println("去重之后的旧数组："+JSON.toJSONString(array));
        System.out.println("去重之后的新数组："+JSON.toJSONString(newArray));
    }

    private static int isEq(int i, int[] array) {
        if(i >=array.length-1) return i;
        if (array[i] == array[i + 1]) {
            return isEq(i+1, array);
        } else {
            return i;
        }
    }
}
