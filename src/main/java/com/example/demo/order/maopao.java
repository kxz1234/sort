package com.example.demo.order;

import com.alibaba.fastjson.JSON;

public class maopao {
    public static void main(String[] args) {
        ArrayUtil arrayUtil = new ArrayUtil();
        int[] array = arrayUtil.getArray(10);
        System.out.println("一开始：" + JSON.toJSONString(array));
        for (int k = 0; k < array.length - 1; k++) {
            for (int i = 0; i < array.length-1-k; i++) {
                if (array[i] > array[i + 1]) {
                    swap(i,i+1,array);
                }
                System.out.println(i+":" + JSON.toJSONString(array));
            }
        }
        System.out.println("最后的数组：" + JSON.toJSONString(array));
    }

    public static void swap(int a, int b, int[] array) {
        int temp = array[a];
        array[a] = array[b];
        array[b] = temp;
    }
}
