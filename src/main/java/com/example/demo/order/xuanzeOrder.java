package com.example.demo.order;

import com.alibaba.fastjson.JSON;

public class xuanzeOrder {
    public static void main(String[] args) {
        ArrayUtil arrayUtil = new ArrayUtil();
        int[] array = arrayUtil.getArray(10);
        for (int i = 0; i < array.length; i++) {
            int min = i;
            for (int k = i + 1; k < array.length; k++) {
                System.out.println(k);
                if (array[min] > array[k]) {
                    min = k;
                }
            }
            if (min != i) {
                arrayUtil.swap(min, i, array);
            }
        }
        System.out.println("最后的数组为：" + JSON.toJSONString(array));
    }
}
